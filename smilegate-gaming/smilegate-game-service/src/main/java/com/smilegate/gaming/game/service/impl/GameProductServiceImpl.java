/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.smilegate.gaming.game.service.impl;

import com.smilegate.gaming.game.model.GameProduct;
import com.smilegate.gaming.game.repository.GameProductRepository;
import com.smilegate.gaming.game.service.GameProductService;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author THAIN
 */
@Service
public class GameProductServiceImpl implements GameProductService {

    @Autowired
    private GameProductRepository gameProductRepository;

    @Override
    public Page<GameProduct> getGameProductPage(String keyword, String category, String genre, String tag, Long minPrice, Long maxPrice, int page, int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("name").ascending());
        keyword = "%" + keyword + "%";
        if (tag != null) {
            tag = "%" + tag + "%";
        }
        return gameProductRepository.getGameProductPage(keyword, category, genre, tag, minPrice, maxPrice, pageable);
    }

    @Override
    public GameProduct findById(String id) {
        Optional<GameProduct> optional = gameProductRepository.findById(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            return null;
        }
    }

    @Override
    public List<GameProduct> findByIdIn(List<String> ids) {
        return gameProductRepository.findByIdIn(ids);
    }

}
