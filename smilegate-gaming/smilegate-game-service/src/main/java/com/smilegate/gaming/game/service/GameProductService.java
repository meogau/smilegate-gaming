/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.smilegate.gaming.game.service;

import com.smilegate.gaming.game.model.GameProduct;
import java.util.List;
import org.springframework.data.domain.Page;

/**
 *
 * @author THAIN
 */
public interface GameProductService {

    Page<GameProduct> getGameProductPage(String keyword, String category, String genre, String tag, Long minPrice, Long maxPrice, int page, int size);

    GameProduct findById(String id);

    List<GameProduct> findByIdIn(List<String> ids);
}
