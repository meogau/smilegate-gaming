package com.smilegate.gaming.game.messaging.rabbitmq;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.smilegate.gaming.game.controller.GameController;
import com.smilegate.gaming.game.exception.ValidationException;
import com.thai.nv.constant.ResourcePath;
import com.thai.nv.message.RequestMessage;
import com.thai.nv.message.ResponseMessage;
import com.thai.nv.utils.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.http.HttpStatus;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author ThaiN
 */
public class RpcServer {

    private static final Logger LOGGER = LoggerFactory.getLogger(RpcServer.class);

    @Autowired
    private GameController gameController;

    @RabbitListener(queues = "${game.rpc.queue}")
    public String processService(String json) throws ValidationException {
        try {
            LOGGER.info(" [-->] Server received request for " + json);

            ObjectMapper mapper = new ObjectMapper();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            mapper.setDateFormat(df);
            RequestMessage request = mapper.readValue(json, RequestMessage.class);
            ResponseMessage response = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase(), null);
            if (request != null) {
                String requestPath = request.getRequestPath().replace(request.getVersion() != null
                        ? request.getVersion() : ResourcePath.VERSION, "");
                Map<String, Object> bodyParam = request.getBodyParam();
                Map<String, String> headerParam = request.getHeaderParam();
                String urlParam = null;
                if (request.getUrlParam() != null) {
                    urlParam = URLDecoder.decode(request.getUrlParam(), StandardCharsets.UTF_8.name());
                }
                String pathParam = request.getPathParam();
                switch (request.getRequestMethod()) {
                    case "GET":
                        if ("/game".equalsIgnoreCase(requestPath)) {
                            if (StringUtil.isNullOrEmpty(pathParam)) {
                                response = gameController.getGameProductPage(headerParam, requestPath, urlParam);
                            } else {
                                response = gameController.getDetailGame(headerParam, pathParam, urlParam);
                            }
                        }
                        break;
                    case "POST":
                        if ("/game/list".equalsIgnoreCase(requestPath)) {
                            response = gameController.getGameList(bodyParam);
                        }
                        break;
                    case "PUT":
                        break;
                    case "PATCH":
                        break;
                    case "DELETE":
                        break;
                    default:
                        break;
                }
            }
            LOGGER.info(" [<--] Server returned " + response != null ? response.toJsonString() : "null");
            return response != null ? response.toJsonString() : null;
        } catch (Exception ex) {
            LOGGER.error("Error to processService >>> " + StringUtil.printException(ex));
            ex.printStackTrace();
        }

        return null;
    }
}
