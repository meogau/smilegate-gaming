package com.smilegate.gaming.game;

//import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableCaching
//@EnableScheduling
public class GameServiceApplication {
    
    public static void main(String[] args) {
        // Fix lỗi "UDP failed setting ip_ttl | Method not implemented" khi start app trên Windows
        System.setProperty("java.net.preferIPv4Stack", "true");
        
        System.out.println(System.getProperty("java.io.tmpdir"));

        //SpringApplication.run(productServiceApplication.class, args);
        new SpringApplicationBuilder(GameServiceApplication.class).web(WebApplicationType.NONE).run(args);
    }
}
