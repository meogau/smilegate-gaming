package com.smilegate.gaming.game.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.smilegate.gaming.game.model.GameProduct;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import com.smilegate.gaming.game.model.dto.AuthorizationResponseDTO;
import com.smilegate.gaming.game.service.GameProductService;
import com.thai.nv.message.MessageContent;
import com.thai.nv.message.ResponseMessage;
import com.thai.nv.utils.StringUtil;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

@Controller
public class GameController extends BaseController {

    @Autowired
    private GameProductService gameProductService;

    public ResponseMessage getGameProductPage(Map<String, String> headerParam, String requestPath, String urlParam) {
        ResponseMessage response;
        Map<String, String> urlMap = StringUtil.getUrlParamValues(urlParam);
        int page = urlMap.get("page") != null ? Integer.parseInt(urlMap.get("page").toString()) : 0;
        int size = urlMap.get("size") != null ? Integer.parseInt(urlMap.get("size").toString()) : 10;
        Long minPrice = urlMap.get("minPrice") != null ? Long.parseLong(urlMap.get("minPrice").toString()) : null;
        Long maxPrice = urlMap.get("maxPrice") != null ? Long.parseLong(urlMap.get("maxPrice").toString()) : null;
        String keyword = urlMap.get("keyword") != null ? urlMap.get("keyword") : "";
        String category = urlMap.get("category");
        String genre = urlMap.get("genre");
        String tag = urlMap.get("tag");
        Page<GameProduct> gameProductPage
                = gameProductService.getGameProductPage(keyword, category, genre, tag, minPrice, maxPrice, page, size);
        response = new ResponseMessage(
                new MessageContent(HttpStatus.OK.value(), HttpStatus.OK.toString(), gameProductPage.getContent(), gameProductPage.getTotalElements()));
        return response;
    }

    public ResponseMessage getDetailGame(Map<String, String> headerParam, String pathParam, String urlParam) {
        ResponseMessage response;
        GameProduct gameProduct = gameProductService.findById(pathParam);
        if (gameProduct == null) {
            response = new ResponseMessage(HttpStatus.NOT_FOUND.value(), "Không tìm thấy game với id " + pathParam,
                    new MessageContent(HttpStatus.NOT_FOUND.value(), "Không tìm thấy game với id " + pathParam, null));
        } else {
            response = new ResponseMessage(new MessageContent(gameProduct));
        }

        return response;
    }

    public ResponseMessage getGameList(Map<String, Object> bodyRequest) {
        ObjectMapper mapper = new ObjectMapper();
        List<String> listGameIds = mapper.convertValue(bodyRequest.get("listGameIds"), new TypeReference<List<String>>() {
        });
        ResponseMessage response = new ResponseMessage(new MessageContent(gameProductService.findByIdIn(listGameIds)));

        return response;
    }

}
