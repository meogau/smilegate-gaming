/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.smilegate.gaming.game.repository;

import com.smilegate.gaming.game.model.GameProduct;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author THAIN
 */
@Repository
public interface GameProductRepository extends PagingAndSortingRepository<GameProduct, String> {

    @Query(value = "select p from GameProduct p where (lower(p.name) like lower(:keyword) or lower(p.code) like lower(:keyword))"
            + " and (:category is null or p.category = :category)"
            + " and (:genre is null or p.genre =:genre)"
            + " and (:tag is null or p.tag like tag)"
            + " and (:minPrice is null or p.price >=:minPrice)"
            + " and (:maxPrice is null or p.price <=:maxPrice)"
    )

    Page<GameProduct> getGameProductPage(String keyword, String category, String genre, String tag, Long minPrice, Long maxPrice, Pageable paging);

    List<GameProduct> findByIdIn(List<String> ids);
}
