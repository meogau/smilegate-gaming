/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smilegate.gaming.id.service.impl;

import com.smilegate.gaming.id.model.dto.TokenCache;
import com.smilegate.gaming.id.repository.RedisRepository;
import com.smilegate.gaming.id.service.TokenService;
import com.smilegate.gaming.id.utils.JWTutils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Thainv
 */
@Service
@Transactional
@EnableAsync
public class TokenServiceImpl implements TokenService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TokenServiceImpl.class);

    private final String JWT_SECRET = "elcom@123_2020";

    private final static String SECRET_KEY = "elcom_wq3Dr8O5wrkCSybDkQ==1_2020@)@)";

    private final long ACCESS_JWT_EXPIRATION = 604800000L;//7 day

    @Autowired
    private RedisRepository redisRepository;

    @Override
    public String setRefreshTokenUpdate(String uuid) {
        String refreshToken = JWTutils.createToken(uuid);
        TokenCache tokenCache = new TokenCache();
        tokenCache.setUuid(uuid);
        tokenCache.setToken(refreshToken);
        redisRepository.saveRefreshTokenRedis(tokenCache);
        return refreshToken;
    }

    @Override
    public String getRefreshTokenUpdate(String uuid) {
        return redisRepository.findUuidRefreshTokenRedis(uuid).getToken();
    }

    @Override
    public boolean removeRefreshTokenUpdate(String uuid) {
        redisRepository.removeRefreshTokenRedis(uuid);
        return true;
    }
}
