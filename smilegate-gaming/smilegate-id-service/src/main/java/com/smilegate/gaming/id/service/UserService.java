package com.smilegate.gaming.id.service;

import com.smilegate.gaming.id.model.User;
//import java.util.Optional;

/**
 *
 * @author anhdv
 */
public interface UserService {

    User findByUuid(String uuid);

    User findByEmail(String email);

    User findByMobile(String mobile);

    User findByUserName(String userName);
    
    User findByEmailOrMobileOrUserName(String keyword);

    void save(User user);
}
