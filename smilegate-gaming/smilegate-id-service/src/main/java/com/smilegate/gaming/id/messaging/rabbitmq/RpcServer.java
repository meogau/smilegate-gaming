package com.smilegate.gaming.id.messaging.rabbitmq;

import com.thai.nv.constant.ResourcePath;
import com.thai.nv.message.RequestMessage;
import com.thai.nv.message.ResponseMessage;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.smilegate.gaming.id.controller.AuthenController;
import com.smilegate.gaming.id.controller.UserController;
import com.smilegate.gaming.id.exception.ValidationException;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Map;

/**
 * @author ThaiN
 */
public class RpcServer {

    private static final Logger LOGGER = LoggerFactory.getLogger(RpcServer.class);

    @Autowired
    private AuthenController authenController;

    @Autowired
    private UserController userController;

    @RabbitListener(queues = "${user.rpc.queue}", containerFactory = "rabbitListenerContainerFactory")
    public String processService(String json) throws ValidationException {
        try {
            LOGGER.info(" [-->] Server received request for " + json);
            ObjectMapper mapper = new ObjectMapper();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            mapper.setDateFormat(df);
            RequestMessage request = mapper.readValue(json, RequestMessage.class);

            ResponseMessage response = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase(), null);
            if (request != null) {
                String requestPath = request.getRequestPath().replace(request.getVersion() != null
                        ? request.getVersion() : ResourcePath.VERSION, "");
                String urlParam = request.getUrlParam();
                String pathParam = request.getPathParam();
                Map<String, Object> bodyParam = request.getBodyParam();
                Map<String, String> headerParam = request.getHeaderParam();

                switch (request.getRequestMethod()) {
                    case "GET":
                        
                        break;
                    case "POST":
                        if ("/user/login".equalsIgnoreCase(requestPath)) {
                            response = authenController.userLogin(requestPath, headerParam, bodyParam);
                        } else if ("/user/refreshToken".equalsIgnoreCase(requestPath)) {
                            response = authenController.refreshToken(headerParam);
                        } else if ("/user/authentication".equalsIgnoreCase(requestPath)) {
                            response = authenController.authorized(requestPath, headerParam);
                        } 
                        break;
                    case "PUT":
                        break;
                    case "PATCH":
                        break;
                    case "DELETE":
                        break;
                    default:
                        break;
                }
            }
            LOGGER.info(" [<--] Server returned {}", (response != null ? response.toJsonString() : "null"));
            return response != null ? response.toJsonString() : null;
        } catch (Exception ex) {
            LOGGER.error("Error to processService >>> {}", ExceptionUtils.getStackTrace(ex));
            ex.printStackTrace();
        }
        return null;
    }
}
