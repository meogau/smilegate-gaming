package com.smilegate.gaming.id.auth.jwt;

import com.smilegate.gaming.id.auth.CustomUserDetails;
import com.smilegate.gaming.id.service.TokenService;
import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author anhdv
 */
@Component
public class JwtTokenProvider {

    @Autowired
    private static final Logger LOGGER = LoggerFactory.getLogger(JwtTokenProvider.class);
    private final String JWT_SECRET = "thainv@123_2020";
    private final long ACCESS_JWT_EXPIRATION = 604800000L;//7 day

    public String generateToken(CustomUserDetails userDetails) {
        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + ACCESS_JWT_EXPIRATION);
        return Jwts.builder()
                   .setSubject(userDetails.getUser().getUuid())
                   .setIssuedAt(now)
                   .setExpiration(expiryDate)
                   .signWith(SignatureAlgorithm.HS512, JWT_SECRET)
                   .compact();
    }
    public String generateToken1(String uuid) {
        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + ACCESS_JWT_EXPIRATION);
        return Jwts.builder()
                .setSubject(uuid)
                .setIssuedAt(now)
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, JWT_SECRET)
                .compact();
    }
    public Boolean checkTokenExpired(String token){
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(JWT_SECRET)
                    .parseClaimsJws(token)
                    .getBody();
            return false;
        } catch (Exception ex){
            return true;
        }
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(JWT_SECRET).parseClaimsJws(authToken);
            
            return true;
        } catch (MalformedJwtException ex) {
            LOGGER.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            LOGGER.error("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            LOGGER.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            LOGGER.error("JWT claims string is empty.");
        }
        return false;
    }
    public static void main(String[] args) {
        String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJkZDYwOWE0ZC0zYWZmLTRlZWYtYTUzYy05ZmE3NmQ2NjlhMDMiLCJpYXQiOjE2MDkxMjk0MzMsImV4cCI6MTYwOTczNDIzM30.v_NtMslWCT6RgDl6t8qX4JBu058pkBve96OqW6pdjKGTeLK4H0ASpj4lA13uhH_DSOf0wGAHVUiXNiHMnvFSjw";
    }

    public String getUuidFromJWT(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(JWT_SECRET)
                .parseClaimsJws(token)
                .getBody();

        return claims.getSubject();
    }
}
