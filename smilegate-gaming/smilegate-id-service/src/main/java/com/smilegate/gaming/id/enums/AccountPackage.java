package com.smilegate.gaming.id.enums;

/**
 * @author hanh
 */
public enum AccountPackage {

    DEFAULT(0, "DEFAULT"),
    PROFESSIONAL(1, "PROFESSIONAL"),
    BLOCKED(2, "BLOCKED");

    private int code;
    private String description;

    AccountPackage(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int code() {
        return code;
    }

    public String description() {
        return description;
    }

    public static AccountPackage of(int code) {
        AccountPackage[] validFlags = AccountPackage.values();
        for (AccountPackage validFlag : validFlags) {
            if (validFlag.code() == code) {
                return validFlag;
            }
        }
        return DEFAULT;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
