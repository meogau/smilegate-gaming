package com.smilegate.gaming.id.validation;

import com.thai.nv.utils.StringUtil;
import com.smilegate.gaming.id.exception.ValidationException;
import com.smilegate.gaming.id.model.User;
import com.smilegate.gaming.id.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class UserValidation extends AbstractValidation {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserValidation.class);

    @Autowired
    private UserService userService;

    public String validateInsertUser(User user) {
        if (user == null) {
            return "PayLoad không hợp lệ";
        }
        if (StringUtil.isNullOrEmpty(user.getEmail())
                && StringUtil.isNullOrEmpty(user.getMobile())) {
            getMessageDes().add("Email hoặc số điện thoại không được để trống");
        }

        if (!StringUtil.isNullOrEmpty(user.getEmail()) && !StringUtil.validateEmail(user.getEmail())) {
            getMessageDes().add("Email không đúng định dạng");
        }

        if (!StringUtil.isNullOrEmpty(user.getMobile()) && !StringUtil.checkMobilePhoneNumberNew(user.getMobile())) {
            getMessageDes().add("Số điện thoại không đúng định dạng");
        }

        if (StringUtil.isNullOrEmpty(user.getFullName())) {
            getMessageDes().add("Tên đầy đủ không được để trống");
        }

        if (StringUtil.isNullOrEmpty(user.getPassword()) || StringUtil.isNullOrEmpty(user.getMatchingPassword())) {
            getMessageDes().add("Mật khẩu không được để trống");
        } else if (!user.getPassword().equals(user.getMatchingPassword())) {
            getMessageDes().add("Mật khẩu và Nhập lại mật khẩu không trùng nhau");
        }

        return !isValid() ? this.buildValidationMessage() : null;
    }

    

    public String validateLogin(String userInfo, String password) throws ValidationException {

        if (StringUtil.isNullOrEmpty(userInfo)) {
            getMessageDes().add("Email/Số điện thoại không được để trống");
        }
        boolean isValidUserInfo = false;
        if (StringUtil.validateEmail(userInfo)) {
            isValidUserInfo = true;
        }
        if (StringUtil.checkMobilePhoneNumberNew(userInfo)) {
            isValidUserInfo = true;
        }
        if (!isValidUserInfo) {
        }

        if (StringUtil.isNullOrEmpty(password)) {
            getMessageDes().add("Mật khẩu không được để trống");
        }

        return !isValid() ? this.buildValidationMessage() : null;
    }

}
