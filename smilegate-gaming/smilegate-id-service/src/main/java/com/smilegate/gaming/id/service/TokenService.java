/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smilegate.gaming.id.service;

/**
 *
 * @author ThaiN
 */
public interface TokenService {

    String getRefreshTokenUpdate(String uuid);

    String setRefreshTokenUpdate(String uuid);

    boolean removeRefreshTokenUpdate(String uuid);

}
