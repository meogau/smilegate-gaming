package com.smilegate.gaming.id.repository;


import com.smilegate.gaming.id.model.User;
import java.util.List;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ThaiN
 */
@Repository
public interface UserRepository extends PagingAndSortingRepository<User, String>, JpaSpecificationExecutor<User> {

    @Override
    List<User> findAll();
    
    User findByUserName(String username);
    
    User findByEmail(String username);
    
    User findByMobile(String username);

}
