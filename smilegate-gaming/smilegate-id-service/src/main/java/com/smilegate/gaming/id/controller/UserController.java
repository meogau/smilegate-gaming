package com.smilegate.gaming.id.controller;

import com.smilegate.gaming.id.model.User;
import com.smilegate.gaming.id.service.UserService;
import com.thai.nv.message.MessageContent;
import com.thai.nv.message.ResponseMessage;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.smilegate.gaming.id.model.dto.AuthorizationResponseDTO;
import com.smilegate.gaming.id.validation.UserValidation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import java.util.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author anhdv
 */
@Controller
public class UserController extends BaseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    public ResponseMessage createUser(String requestUrl, String method, Map<String, String> headerParam, Map<String, Object> bodyMap) {
        ResponseMessage response;
        AuthorizationResponseDTO userDTO = getAuthorFromToken(headerParam);
        if (userDTO == null) {
            response = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), "Bạn chưa đăng nhập",
                    new MessageContent(HttpStatus.UNAUTHORIZED.value(), "Bạn chưa đăng nhập", null));
        } else {
            User user = createUserRecord(bodyMap);
            user.setUuid(UUID.randomUUID().toString());
            user.setCreatedAt(new Date());
            String invalidString = new UserValidation().validateInsertUser(user);
            if (invalidString != null) {
                response = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), invalidString,
                        new MessageContent(HttpStatus.BAD_REQUEST.value(), invalidString, null));
            } else {
                invalidString = checkExistedUser(user);
                if (invalidString != null) {
                    response = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), invalidString,
                            new MessageContent(HttpStatus.BAD_REQUEST.value(), invalidString, null));
                } else {
                    user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
                    userService.save(user);
                    response = new ResponseMessage(new MessageContent(user));
                }
            }

        }
        return response;
    }

    private User createUserRecord(Map<String, Object> bodyMap) {
        ObjectMapper mapper = new ObjectMapper();
        User user = mapper.convertValue(bodyMap, new TypeReference<User>() {
        });
        user.setStatus(1);
        String password = bodyMap.get("password") != null ? (String) bodyMap.get("password") : null;
        String matchingPassword = bodyMap.get("matchingPassword") != null ? (String) bodyMap.get("matchingPassword") : null;
        user.setPassword(password);
        user.setMatchingPassword(matchingPassword);
        return user;
    }

    private String checkExistedUser(User user) {
        if (userService.findByUserName(user.getUserName()) != null) {
            return "Tên đăng nhập đã được sử dụng";

        } else if (userService.findByMobile(user.getMobile()) != null) {
            return "Số điện thoại đã được sử dụng";

        } else if (userService.findByEmail(user.getEmail()) != null) {
            return "Email đã được sử dụng";

        }
        return null;
    }
}
