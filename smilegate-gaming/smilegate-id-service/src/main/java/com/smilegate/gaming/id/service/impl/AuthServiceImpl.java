package com.smilegate.gaming.id.service.impl;

import com.smilegate.gaming.id.auth.CustomUserDetails;
import com.smilegate.gaming.id.model.User;
import com.smilegate.gaming.id.repository.UserRepository;
import com.smilegate.gaming.id.service.AuthService;

import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 *
 * @author anhdv
 */
@Service
public class AuthServiceImpl implements UserDetailsService, AuthService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthServiceImpl.class);
    
    @Autowired
    private UserRepository userRepository;


    @Override
    public UserDetails loadUserByUsername(String userInfo) {
        User user = userRepository.findByEmail(userInfo);
        if (user == null) {
            user = userRepository.findByMobile(userInfo);
        }
        if (user == null) {
            user = userRepository.findByUserName(userInfo);
        }
        if (user == null) {
            throw new UsernameNotFoundException("User not found with userInfo : " + userInfo);
        } else {
            LOGGER.info("Find user with " + userInfo + " ==> uuid: " + user.getUuid());
        }
        return new CustomUserDetails(user);
    }

    // JWTAuthenticationFilter sẽ sử dụng hàm này
    @Transactional
    @Override
    public UserDetails loadUserByUuid(String uuid) {
        User user = userRepository.findById(uuid).get();
        if (user == null) {
            throw new UsernameNotFoundException("User not found with uuid : " + uuid);
        }
        return new CustomUserDetails(user);
    }
}
