package com.smilegate.gaming.id;

//import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableCaching
@EnableScheduling
@EnableAsync
public class IDServiceApplication {
    
    public static void main(String[] args) {

//        System.setProperty("java.io.tmpdir", "D:\\tmp1");
        // Fix lỗi "UDP failed setting ip_ttl | Method not implemented" khi start app trên Windows
        System.setProperty("java.net.preferIPv4Stack", "true");

        //SpringApplication.run(IDServiceApplication.class, args);
        new SpringApplicationBuilder(IDServiceApplication.class).web(WebApplicationType.NONE).run(args);
    }
}
