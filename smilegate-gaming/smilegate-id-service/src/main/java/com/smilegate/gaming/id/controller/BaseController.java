package com.smilegate.gaming.id.controller;

import com.smilegate.gaming.id.auth.CustomUserDetails;
import com.smilegate.gaming.id.auth.jwt.JwtTokenProvider;
import com.smilegate.gaming.id.auth.jwt.TokenParse;
import com.smilegate.gaming.id.utils.JWTutils;
import com.thai.nv.message.MessageContent;
import com.thai.nv.message.ResponseMessage;
import com.smilegate.gaming.id.model.User;
import com.smilegate.gaming.id.model.dto.AuthorizationResponseDTO;
import com.smilegate.gaming.id.service.AuthService;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;

/**
 *
 * @author ThaiN
 */
public class BaseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseController.class);

    @Autowired
    private JwtTokenProvider tokenProvider;

    @Autowired
    private JWTutils jwTutils;

    @Autowired
    private AuthService authService;

 
    public ResponseMessage getAuthorFromRefreshToken(Map<String, String> headerParam) {
        if (headerParam == null || (!headerParam.containsKey("authorization")
                && !headerParam.containsKey("Authorization"))) {
            return new ResponseMessage(new MessageContent(HttpStatus.UNAUTHORIZED.value(), "failed authentication", null));
        }
        String bearerToken = headerParam.get("authorization");
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            try {
                String jwt = bearerToken.substring(7);
                TokenParse tokenParse = jwTutils.getContentInToken(jwt);
                return new ResponseMessage(new MessageContent(tokenParse.getCodeStatus(), tokenParse.getMessage(), tokenParse.getData()));

            } catch (Exception ex) {
                LOGGER.error("failed on set user authentication", ex);
                return new ResponseMessage(new MessageContent(HttpStatus.UNAUTHORIZED.value(), "failed on set user authentication", null));
            }
        }
        return new ResponseMessage(new MessageContent(HttpStatus.UNAUTHORIZED.value(), "failed Bearer authentication", null));
    }

    public AuthorizationResponseDTO getAuthorFromToken(Map<String, String> headerParam) {
        if (headerParam == null || (!headerParam.containsKey("authorization")
                && !headerParam.containsKey("Authorization"))) {
            return null;
        }
        String bearerToken = headerParam.get("authorization");
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            try {
                String jwt = bearerToken.substring(7);
                String uuid = tokenProvider.getUuidFromJWT(jwt);
                UserDetails userDetails = authService.loadUserByUuid(uuid);
                if (userDetails != null) {
                    User user = ((CustomUserDetails) userDetails).getUser();
                    if (user.getStatus() == User.STATUS_LOCK) {
                        return null;
                    } else {
                        UsernamePasswordAuthenticationToken authentication
                                = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                        SecurityContextHolder.getContext().setAuthentication(authentication);
                        AuthorizationResponseDTO responseDTO = new AuthorizationResponseDTO((CustomUserDetails) authentication.getPrincipal(), null, null);
                        return responseDTO;
                    }
                }
            } catch (Exception ex) {
                LOGGER.error("failed on set user authentication", ex);
                return null;
            }
        }
        return null;
    }

    
}
