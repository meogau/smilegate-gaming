package com.smilegate.gaming.id.service.impl;

import com.smilegate.gaming.id.model.User;
import com.smilegate.gaming.id.repository.UserRepository;
import com.smilegate.gaming.id.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Thainv
 */
@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
    @Autowired
    private UserRepository userRepository;

    public long countAll() {
        return userRepository.count();
    }

    @Override
    public User findByUuid(String uuid) {
        return userRepository.findById(uuid).orElse(null);
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public User findByEmail(String email) {
        try {
            return userRepository.findByEmail(email);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
        }
        return null;
    }

    @Override
    public User findByMobile(String mobile) {
        return userRepository.findByMobile(mobile);
    }

    @Override
    public User findByUserName(String userName) {
        return userRepository.findByUserName(userName);
    }

    @Override
    public User findByEmailOrMobileOrUserName(String keyword) {
        User user = null;
        user = userRepository.findByUserName(keyword);
        if (user == null) {
            user = userRepository.findByEmail(keyword);
        }
        if (user == null) {
            user = userRepository.findByMobile(keyword);
        }
        return user;
    }

}
