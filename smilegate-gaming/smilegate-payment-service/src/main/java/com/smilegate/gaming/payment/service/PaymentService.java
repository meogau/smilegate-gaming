/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.smilegate.gaming.payment.service;

import com.smilegate.gaming.payment.model.dto.PaymentRequest;

/**
 *
 * @author THAIN
 */
public interface PaymentService {

    boolean processPayment(PaymentRequest paymentRequest);
}
