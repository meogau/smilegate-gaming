/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.smilegate.gaming.payment.service.impl;

import com.smilegate.gaming.payment.enums.PaymentStatus;
import com.smilegate.gaming.payment.model.PaymentData;
import com.smilegate.gaming.payment.model.dto.PaymentRequest;
import com.smilegate.gaming.payment.repository.PaymentRepository;
import com.smilegate.gaming.payment.service.PaymentService;
import com.smilegate.gaming.payment.service.method.CashPayment;
import com.smilegate.gaming.payment.service.method.PaymentMethod;
import java.util.Date;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author THAIN
 */
@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private PaymentRepository paymentRepository;

    @Override
    public boolean processPayment(PaymentRequest paymentRequest) {
        PaymentData paymentData = createPaymentData(paymentRequest);
        PaymentMethod paymentMethod = paymentData.getPaymentMethod();
        if (paymentMethod.pay(paymentData.getMoneyToPay())) {
            paymentData.setStatus(PaymentStatus.SUCCESS);
            paymentRepository.save(paymentData);
            return true;
        } else {
            paymentData.setStatus(PaymentStatus.FAIL);
            paymentRepository.save(paymentData);
            return false;
        }
    }

    PaymentData createPaymentData(PaymentRequest paymentRequest) {
        PaymentData paymentData = new PaymentData(UUID.randomUUID().toString());
        paymentData.setOrderId(paymentRequest.getOrderId());
        paymentData.setDiscount(paymentRequest.getDiscount());
        paymentData.setMoneyToPay(paymentRequest.getMoneyToPay());
        paymentData.setPaymentMethodString(paymentRequest.getPaymentMethod());
        paymentData.setSurfax(paymentRequest.getSurtax());
        paymentData.setTotalAmount(paymentRequest.getTotalAmount());
        paymentData.setCreatedDate(new Date());
        switch (paymentData.getPaymentMethodString()) {
            case "CASH":
                paymentData.setPaymentMethod(new CashPayment());
                break;
            default:
                paymentData.setPaymentMethod(new CashPayment());
        }
        return paymentData;
    }

}
