/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.smilegate.gaming.payment.repository;

import com.smilegate.gaming.payment.model.PaymentData;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author THAIN
 */
@Repository
public interface PaymentRepository extends CrudRepository<PaymentData, String> {

}
