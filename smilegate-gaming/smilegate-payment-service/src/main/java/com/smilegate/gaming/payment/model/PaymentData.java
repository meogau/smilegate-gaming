/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.smilegate.gaming.payment.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.smilegate.gaming.payment.enums.PaymentStatus;
import com.smilegate.gaming.payment.service.method.PaymentMethod;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author THAIN
 */
@Entity
@Table(name = "payment")
@NamedQueries({
    @NamedQuery(name = "PaymentData.findAll", query = "SELECT p FROM PaymentData p")})
public class PaymentData implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 36)
    @Column(name = "id")
    private String id;
    @Size(max = 36)
    @Column(name = "order_id")
    private String orderId;
    @Column(name = "total_amount")
    private Long totalAmount;
    @Column(name = "surfax")
    private Long surfax;
    @Column(name = "discount")
    private Long discount;
    @Column(name = "money_to_pay")
    private Long moneyToPay;
    @Size(max = 255)
    @Column(name = "payment_method")
    private String paymentMethodString;
    @Column(name = "created_date")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+7")
    private Date createdDate;
    @Column(name = "status")
    private PaymentStatus status;
    
    @Transient
    PaymentMethod paymentMethod;

    public PaymentData() {
    }

    public PaymentData(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Long getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Long totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Long getSurfax() {
        return surfax;
    }

    public void setSurfax(Long surfax) {
        this.surfax = surfax;
    }

    public Long getDiscount() {
        return discount;
    }

    public void setDiscount(Long discount) {
        this.discount = discount;
    }

    public Long getMoneyToPay() {
        return moneyToPay;
    }

    public void setMoneyToPay(Long moneyToPay) {
        this.moneyToPay = moneyToPay;
    }

    public String getPaymentMethodString() {
        return paymentMethodString;
    }

    public void setPaymentMethodString(String paymentMethodString) {
        this.paymentMethodString = paymentMethodString;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public PaymentStatus getStatus() {
        return status;
    }

    public void setStatus(PaymentStatus status) {
        this.status = status;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PaymentData)) {
            return false;
        }
        PaymentData other = (PaymentData) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.smilegate.gaming.payment.model.PaymentData[ id=" + id + " ]";
    }
    
}
