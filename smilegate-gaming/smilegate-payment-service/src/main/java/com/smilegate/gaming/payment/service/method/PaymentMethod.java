/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.smilegate.gaming.payment.service.method;

/**
 *
 * @author THAIN
 */
public interface PaymentMethod {

    boolean pay(long moneyToPay);
}
