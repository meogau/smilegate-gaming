/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smilegate.gaming.payment.enums;

/**
 *
 * @author Admin
 */
public enum PaymentStatus {
    FAIL(0, "Lỗi"),
    SUCCESS(1, "Đã hoàn thành");

    private final int code;
    private final String description;

    public int code() {
        return code;
    }

    public String description() {
        return description;
    }

    PaymentStatus(int code, String description) {
        this.code = code;
        this.description = description;
    }

}
