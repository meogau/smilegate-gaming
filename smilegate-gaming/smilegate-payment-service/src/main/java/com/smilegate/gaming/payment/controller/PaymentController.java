package com.smilegate.gaming.payment.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.smilegate.gaming.payment.model.dto.PaymentRequest;
import com.smilegate.gaming.payment.service.PaymentService;
import org.springframework.stereotype.Controller;
import com.thai.nv.message.MessageContent;
import com.thai.nv.message.ResponseMessage;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

@Controller
public class PaymentController extends BaseController {

    @Autowired
    private PaymentService paymentService;

    public ResponseMessage payOrder(Map<String, Object> bodyRequest) {
        ResponseMessage response;
        ObjectMapper mapper = new ObjectMapper();
        PaymentRequest paymentRequest = mapper.convertValue(bodyRequest.get("paymentRequest"), new TypeReference<PaymentRequest>() {
        });
        if (paymentService.processPayment(paymentRequest)) {
            response = new ResponseMessage(new MessageContent("SUCCESS"));
        } else {
            response = new ResponseMessage(HttpStatus.BAD_GATEWAY.value(), HttpStatus.BAD_GATEWAY.toString(),
                    new MessageContent(HttpStatus.BAD_GATEWAY.value(), HttpStatus.BAD_GATEWAY.toString(), "FAIL"));
        }

        return response;
    }

}
