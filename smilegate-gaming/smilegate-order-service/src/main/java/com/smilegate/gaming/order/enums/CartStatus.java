/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smilegate.gaming.order.enums;

/**
 *
 * @author Admin
 */
public enum CartStatus {
    EMPTY(0, "Rỗng"),
    SHOPPING(1, "Đang mua hàng");

    private final int code;
    private final String description;

    public int code() {
        return code;
    }

    public String description() {
        return description;
    }

    CartStatus(int code, String description) {
        this.code = code;
        this.description = description;
    }


}
