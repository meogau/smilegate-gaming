/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.smilegate.gaming.order.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

/**
 *
 * @author THAIN
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class GameProduct {

    private String id;
    private String name;
    private String code;
    private String description;
    private String image;
    private String category;
    private String genre;
    private String tag;
    private Long price;

    public GameProduct() {
    }
    
    

}
