/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.smilegate.gaming.order.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.smilegate.gaming.order.messaging.rabbitmq.RabbitMQClient;
import com.smilegate.gaming.order.messaging.rabbitmq.RabbitMQProperties;
import com.smilegate.gaming.order.model.dto.GameProduct;
import com.smilegate.gaming.order.service.GameProductService;
import com.thai.nv.constant.ResourcePath;
import com.thai.nv.message.MessageContent;
import com.thai.nv.message.RequestMessage;
import com.thai.nv.message.ResponseMessage;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

/**
 *
 * @author THAIN
 */
@Service
public class GameProductServiceImpl implements GameProductService {

    @Autowired
    private RabbitMQClient rabbitMQClient;

    @Override
    public Map<String, GameProduct> getMapGameProductByListGameIds(List<String> listGameIds) {
        Map<String, Object> bodyRequest = new HashMap<>();
        bodyRequest.put("listGameIds", listGameIds);
        RequestMessage getProductRequest = new RequestMessage();
        getProductRequest.setRequestMethod("POST");
        getProductRequest.setRequestPath(RabbitMQProperties.GAME_RPC_LIST_URL);
        getProductRequest.setVersion(ResourcePath.VERSION);
        getProductRequest.setBodyParam(bodyRequest);
        getProductRequest.setUrlParam(null);
        String result = rabbitMQClient.callRpcService(RabbitMQProperties.GAME_RPC_EXCHANGE,
                RabbitMQProperties.GAME_RPC_QUEUE, RabbitMQProperties.GAME_RPC_KEY, getProductRequest.toJsonString());
        if (result != null) {
            ObjectMapper mapper = new ObjectMapper();
            ResponseMessage response = null;
            try {
                response = mapper.readValue(result, ResponseMessage.class);
            } catch (JsonProcessingException ex) {
                return null;
            }
            if (response != null && response.getStatus() == HttpStatus.OK.value()) {
                try {
                    MessageContent content = response.getData();
                    List<GameProduct> listGameProduct = mapper.convertValue(content.getData(), new TypeReference<List<GameProduct>>() {
                    });
                    return listGameProduct.stream().collect(Collectors.toMap(GameProduct::getId,
                            Function.identity()));
                } catch (Exception ex) {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

}
