/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.smilegate.gaming.order.service.impl;

import com.smilegate.gaming.order.enums.OrderStatus;
import com.smilegate.gaming.order.model.OrderData;
import com.smilegate.gaming.order.model.dto.DetailShoppingCart;
import com.smilegate.gaming.order.model.dto.PaymentRequest;
import com.smilegate.gaming.order.repository.OrderRepository;
import com.smilegate.gaming.order.service.OrderService;
import com.smilegate.gaming.order.service.PaymentService;
import com.smilegate.gaming.order.service.ShoppingCartService;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author THAIN
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private ShoppingCartService shoppingCartService;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private PaymentService paymentService;

    @Override
    public OrderData processOrder(String userId, long discount, long surfax) {
        DetailShoppingCart currentCart = shoppingCartService.getCurrentShoppingCart(userId);
        OrderData order = createOrder(userId, currentCart, discount, surfax);
        orderRepository.save(order);
        shoppingCartService.resetCart(currentCart.getId());
        return order;
    }

    private OrderData createOrder(String userId, DetailShoppingCart currentCart, long discount, long surfax) {
        OrderData order = new OrderData(UUID.randomUUID().toString());
        order.setCreatedDate(new Date());
        order.setDiscount(discount);
        order.setListOrderedGame(currentCart.getListOrderedGame());
        order.setTotalAmount(currentCart.getTotalAmount());
        order.setSurtax(surfax);
        order.setUserId(userId);
        order.setMoneyToPay(order.getTotalAmount() + surfax - discount);
        order.setStatus(OrderStatus.WAIT_FOR_PAYMENT);
        return order;
    }

    @Override
    public Page<OrderData> getPageOrder(String userId, int page, int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("createdDate").descending());
        return orderRepository.findByUserId(userId, pageable);
    }

    @Override
    public String processPayOrder(OrderData orderData, String paymentMethod) {
        PaymentRequest paymentRequest = createPaymentRequest(orderData, paymentMethod);
        if (paymentService.payOrder(paymentRequest)) {
            orderData.setPaymentMethod(paymentMethod);
            orderData.setStatus(OrderStatus.SUCCESS);
            orderData.setLastUpdate(new Date());
            orderRepository.save(orderData);
            return null;
        } else {
            return "Lỗi thanh toán";
        }
    }

    private PaymentRequest createPaymentRequest(OrderData orderData, String paymentMethod) {
        return PaymentRequest.builder()
                .discount(orderData.getDiscount())
                .moneyToPay(orderData.getMoneyToPay())
                .paymentMethod(paymentMethod)
                .surtax(orderData.getSurtax())
                .totalAmount(orderData.getTotalAmount())
                .build();
    }

    @Override
    public OrderData findById(String id) {
        Optional<OrderData> orderOptional = orderRepository.findById(id);
        if (orderOptional.isPresent()) {
            return orderOptional.get();
        }
        return null;
    }

}
