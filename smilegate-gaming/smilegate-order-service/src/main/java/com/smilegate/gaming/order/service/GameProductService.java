/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.smilegate.gaming.order.service;

import com.smilegate.gaming.order.model.dto.GameProduct;
import java.util.List;
import java.util.Map;

/**
 *
 * @author THAIN
 */
public interface GameProductService {
    Map<String, GameProduct> getMapGameProductByListGameIds(List<String> listGameIds);
}
