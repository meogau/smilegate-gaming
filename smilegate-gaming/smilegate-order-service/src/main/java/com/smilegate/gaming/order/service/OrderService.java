/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.smilegate.gaming.order.service;

import com.smilegate.gaming.order.model.OrderData;
import org.springframework.data.domain.Page;

/**
 *
 * @author THAIN
 */
public interface OrderService {

    OrderData processOrder(String userId, long discount, long surfax);

    OrderData findById(String id);

    Page<OrderData> getPageOrder(String userId, int page, int size);

    String processPayOrder(OrderData orderData, String paymentMethod);
}
