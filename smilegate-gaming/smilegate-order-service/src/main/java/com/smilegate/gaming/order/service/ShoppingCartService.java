/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.smilegate.gaming.order.service;

import com.smilegate.gaming.order.model.ShoppingCart;
import com.smilegate.gaming.order.model.dto.AddGameToCartRequest;
import com.smilegate.gaming.order.model.dto.DetailShoppingCart;

/**
 *
 * @author THAIN
 */
public interface ShoppingCartService {

    DetailShoppingCart getCurrentShoppingCart(String userId);

    ShoppingCart handleAddNewGame(String userId, AddGameToCartRequest request);

    void resetCart(String cartId);
}
