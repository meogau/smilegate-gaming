package com.smilegate.gaming.order.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.smilegate.gaming.order.model.ShoppingCart;
import com.smilegate.gaming.order.model.dto.AddGameToCartRequest;
import com.smilegate.gaming.order.model.dto.AuthorizationResponseDTO;
import com.smilegate.gaming.order.model.dto.DetailShoppingCart;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import com.thai.nv.message.MessageContent;
import com.thai.nv.message.ResponseMessage;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import com.smilegate.gaming.order.service.ShoppingCartService;

@Controller
public class ShoppingCartController extends BaseController {

    @Autowired
    private ShoppingCartService shoppingCartService;

    public ResponseMessage getCurrentShoppingCart(Map<String, String> headerParam, String requestPath, String urlParam) {
        ResponseMessage response;
        AuthorizationResponseDTO dto = authenToken(headerParam);
        if (dto == null) {
            response = new ResponseMessage(new MessageContent(HttpStatus.FORBIDDEN.value(), "Bạn chưa đăng nhập", null));
        } else {
            DetailShoppingCart currentShoppingCart = shoppingCartService.getCurrentShoppingCart(dto.getUuid());
            response = new ResponseMessage(new MessageContent(currentShoppingCart));
        }
        return response;
    }

    public ResponseMessage handleAddGameToCart(Map<String, String> headerParam, Map<String, Object> bodyRequest) {
        ResponseMessage response;
        AuthorizationResponseDTO dto = authenToken(headerParam);
        if (dto == null) {
            response = new ResponseMessage(new MessageContent(HttpStatus.FORBIDDEN.value(), "Bạn chưa đăng nhập", null));
        } else {
            ObjectMapper mapper = new ObjectMapper();
            AddGameToCartRequest request = mapper.convertValue(bodyRequest, new TypeReference<AddGameToCartRequest>() {
            });
            ShoppingCart currentShoppingCart = shoppingCartService.handleAddNewGame(dto.getUuid(), request);
            response = new ResponseMessage(new MessageContent(currentShoppingCart));
        }
        return response;
    }

}
