package com.smilegate.gaming.order.messaging.rabbitmq;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.smilegate.gaming.order.controller.OrderController;
import com.smilegate.gaming.order.controller.ShoppingCartController;
import com.smilegate.gaming.order.exception.ValidationException;
import com.thai.nv.constant.ResourcePath;
import com.thai.nv.message.RequestMessage;
import com.thai.nv.message.ResponseMessage;
import com.thai.nv.utils.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.http.HttpStatus;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author ThaiN
 */
public class RpcServer {

    private static final Logger LOGGER = LoggerFactory.getLogger(RpcServer.class);

    @Autowired
    private ShoppingCartController shoppingCartController;

    @Autowired
    private OrderController orderController;

    @RabbitListener(queues = "${order.rpc.queue}")
    public String processService(String json) throws ValidationException {
        try {
            LOGGER.info(" [-->] Server received request for " + json);

            ObjectMapper mapper = new ObjectMapper();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            mapper.setDateFormat(df);
            RequestMessage request = mapper.readValue(json, RequestMessage.class);
            ResponseMessage response = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase(), null);
            if (request != null) {
                String requestPath = request.getRequestPath().replace(request.getVersion() != null
                        ? request.getVersion() : ResourcePath.VERSION, "");
                Map<String, Object> bodyParam = request.getBodyParam();
                Map<String, String> headerParam = request.getHeaderParam();
                String urlParam = null;
                if (request.getUrlParam() != null) {
                    urlParam = URLDecoder.decode(request.getUrlParam(), StandardCharsets.UTF_8.name());
                }
                String pathParam = request.getPathParam();
                switch (request.getRequestMethod()) {
                    case "GET":
                        if ("/order/cart".equalsIgnoreCase(requestPath)) {
                            response = shoppingCartController.getCurrentShoppingCart(headerParam, requestPath, urlParam);
                        } else if ("/order".equalsIgnoreCase(requestPath)) {
                            response = orderController.getOrderHistory(headerParam, urlParam);
                        }
                        break;
                    case "POST":
                        if ("/order/cart".equalsIgnoreCase(requestPath)) {
                            response = shoppingCartController.handleAddGameToCart(headerParam, bodyParam);
                        } else if ("/order".equalsIgnoreCase(requestPath)) {
                            response = orderController.placeOrder(headerParam, bodyParam);
                        }else if ("/order/pay".equalsIgnoreCase(requestPath)) {
                            response = orderController.payOrder(headerParam, bodyParam);
                        }
                        break;
                    case "PUT":
                        break;
                    case "PATCH":
                        break;
                    case "DELETE":
                        break;
                    default:
                        break;
                }
            }
            LOGGER.info(" [<--] Server returned " + response != null ? response.toJsonString() : "null");
            return response != null ? response.toJsonString() : null;
        } catch (Exception ex) {
            LOGGER.error("Error to processService >>> " + StringUtil.printException(ex));
            ex.printStackTrace();
        }

        return null;
    }
}
