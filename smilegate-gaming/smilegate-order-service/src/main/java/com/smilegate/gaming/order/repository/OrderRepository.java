/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.smilegate.gaming.order.repository;

import com.smilegate.gaming.order.model.OrderData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author THAIN
 */
@Repository
public interface OrderRepository extends PagingAndSortingRepository<OrderData, String> {

    Page<OrderData> findByUserId(String userService, Pageable pageable);
}
