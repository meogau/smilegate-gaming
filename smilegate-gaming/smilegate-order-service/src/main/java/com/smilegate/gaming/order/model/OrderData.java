/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.smilegate.gaming.order.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.smilegate.gaming.order.enums.OrderStatus;
import com.smilegate.gaming.order.model.dto.DetailOrderedGame;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

/**
 *
 * @author THAIN
 */
@Entity
@Table(name = "order_data")
@TypeDef(
        name = "jsonb",
        typeClass = JsonBinaryType.class
)
@NamedQueries({
    @NamedQuery(name = "OrderData.findAll", query = "SELECT o FROM OrderData o")})
public class OrderData implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 36)
    @Column(name = "id")
    private String id;
    @Size(max = 36)
    @Column(name = "user_id")
    private String userId;
    @Type(type = "jsonb")
    @Column(name = "list_ordered_game", columnDefinition = "jsonb")
    private List<DetailOrderedGame> listOrderedGame;
    @Column(name = "total_amount")
    private Long totalAmount;
    @Column(name = "surtax")
    private Long surtax;
    @Column(name = "discount")
    private Long discount;
    @Column(name = "money_to_pay")
    private Long moneyToPay;
    @Size(max = 255)
    @Column(name = "payment_method")
    private String paymentMethod;
    @Column(name = "status")
    private OrderStatus status;
    @Column(name = "created_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+7")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "last_update")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+7")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;

    public OrderData() {
    }

    public OrderData(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<DetailOrderedGame> getListOrderedGame() {
        return listOrderedGame;
    }

    public void setListOrderedGame(List<DetailOrderedGame> listOrderedGame) {
        this.listOrderedGame = listOrderedGame;
    }

    public Long getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Long totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Long getSurtax() {
        return surtax;
    }

    public void setSurtax(Long surtax) {
        this.surtax = surtax;
    }

    public Long getDiscount() {
        return discount;
    }

    public void setDiscount(Long discount) {
        this.discount = discount;
    }

    public Long getMoneyToPay() {
        return moneyToPay;
    }

    public void setMoneyToPay(Long moneyToPay) {
        this.moneyToPay = moneyToPay;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrderData)) {
            return false;
        }
        OrderData other = (OrderData) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.smilegate.gaming.order.model.OrderData[ id=" + id + " ]";
    }

}
