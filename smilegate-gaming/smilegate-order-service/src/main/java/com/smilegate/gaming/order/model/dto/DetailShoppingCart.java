/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.smilegate.gaming.order.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.smilegate.gaming.order.enums.CartStatus;
import java.util.Date;
import java.util.List;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;

/**
 *
 * @author THAIN
 */
@Data
public class DetailShoppingCart {

    private String id;
    private List<DetailOrderedGame> listOrderedGame;
    private long totalAmount;
    private CartStatus status;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+7")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;
}
