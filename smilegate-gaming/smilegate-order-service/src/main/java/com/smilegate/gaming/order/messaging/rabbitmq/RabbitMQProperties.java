/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smilegate.gaming.order.messaging.rabbitmq;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author ThaiN
 */
@Component
public class RabbitMQProperties {

    @Value("${user.rpc.authen.url}")
    public static String USER_RPC_AUTHEN_URL;

    @Value("${user.rpc.exchange}")
    public static String USER_RPC_EXCHANGE;

    @Value("${user.rpc.queue}")
    public static String USER_RPC_QUEUE;

    @Value("${user.rpc.key}")
    public static String USER_RPC_KEY;

    @Value("${game.rpc.list.url}")
    public static String GAME_RPC_LIST_URL;

    @Value("${game.rpc.exchange}")
    public static String GAME_RPC_EXCHANGE;

    @Value("${game.rpc.queue}")
    public static String GAME_RPC_QUEUE;

    @Value("${game.rpc.key}")
    public static String GAME_RPC_KEY;

    @Value("${payment.rpc.url}")
    public static String PAYMENT_RPC_URL;

    @Value("${payment.rpc.exchange}")
    public static String PAYMENT_RPC_EXCHANGE;

    @Value("${payment.rpc.queue}")
    public static String PAYMENT_RPC_QUEUE;

    @Value("${payment.rpc.key}")
    public static String PAYMENT_RPC_KEY;

    @Autowired
    public RabbitMQProperties(@Value("${user.rpc.exchange}") String userRpcExchange,
            @Value("${user.rpc.queue}") String userRpcQueue,
            @Value("${user.rpc.key}") String userRpcKey,
            @Value("${user.rpc.authen.url}") String userRpcAuthenUrl,
            @Value("${game.rpc.list.url}") String gameListUrl,
            @Value("${game.rpc.exchange}") String gameRpcExchange,
            @Value("${game.rpc.queue}") String gameRpcQueue,
            @Value("${game.rpc.key}") String gameRpcKey,
            @Value("${payment.rpc.url}") String paymentUrl,
            @Value("${payment.rpc.exchange}") String paymentExchange,
            @Value("${payment.rpc.queue}") String paymentQueue,
            @Value("${payment.rpc.key}") String paymentKey
    ) {

        USER_RPC_EXCHANGE = userRpcExchange;
        USER_RPC_QUEUE = userRpcQueue;
        USER_RPC_KEY = userRpcKey;
        USER_RPC_AUTHEN_URL = userRpcAuthenUrl;

        GAME_RPC_EXCHANGE = gameRpcExchange;
        GAME_RPC_KEY = gameRpcKey;
        GAME_RPC_QUEUE = gameRpcQueue;
        GAME_RPC_LIST_URL = gameListUrl;

        PAYMENT_RPC_EXCHANGE = paymentExchange;
        PAYMENT_RPC_KEY = paymentKey;
        PAYMENT_RPC_QUEUE = paymentQueue;
        PAYMENT_RPC_URL = paymentUrl;

    }
}
