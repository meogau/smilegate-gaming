/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.smilegate.gaming.order.model.dto;

import lombok.Data;

/**
 *
 * @author THAIN
 */
@Data
public class AddGameToCartRequest {

    private String gameId;
    private int quantity;
}
