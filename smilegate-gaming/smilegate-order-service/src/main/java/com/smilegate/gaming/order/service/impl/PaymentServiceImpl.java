/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.smilegate.gaming.order.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.smilegate.gaming.order.messaging.rabbitmq.RabbitMQClient;
import com.smilegate.gaming.order.messaging.rabbitmq.RabbitMQProperties;
import com.smilegate.gaming.order.model.dto.PaymentRequest;
import com.smilegate.gaming.order.service.PaymentService;
import com.thai.nv.constant.ResourcePath;
import com.thai.nv.message.RequestMessage;
import com.thai.nv.message.ResponseMessage;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

/**
 *
 * @author THAIN
 */
@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private RabbitMQClient rabbitMQClient;

    @Override
    public boolean payOrder(PaymentRequest paymentRequest) {
        Map<String, Object> bodyRequest = new HashMap<>();
        bodyRequest.put("paymentRequest", paymentRequest);
        RequestMessage payRequest = new RequestMessage();
        payRequest.setRequestMethod("POST");
        payRequest.setRequestPath(RabbitMQProperties.PAYMENT_RPC_URL);
        payRequest.setVersion(ResourcePath.VERSION);
        payRequest.setBodyParam(bodyRequest);
        payRequest.setUrlParam(null);
        String result = rabbitMQClient.callRpcService(RabbitMQProperties.PAYMENT_RPC_EXCHANGE,
                RabbitMQProperties.PAYMENT_RPC_QUEUE, RabbitMQProperties.PAYMENT_RPC_KEY, payRequest.toJsonString());
        if (result != null) {
            ObjectMapper mapper = new ObjectMapper();
            ResponseMessage response = null;
            try {
                response = mapper.readValue(result, ResponseMessage.class);
            } catch (JsonProcessingException ex) {
                return false;
            }
            if (response != null && response.getStatus() == HttpStatus.OK.value()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}
