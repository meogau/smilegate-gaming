/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smilegate.gaming.order.enums;

/**
 *
 * @author Admin
 */
public enum OrderStatus {
    EMPTY(0, "Rỗng"),
    WAIT_FOR_PAYMENT(1, "Chờ thanh toán"),
    SUCCESS(2, "Đã hoàn thành");

    private final int code;
    private final String description;

    public int code() {
        return code;
    }

    public String description() {
        return description;
    }

    OrderStatus(int code, String description) {
        this.code = code;
        this.description = description;
    }

}
