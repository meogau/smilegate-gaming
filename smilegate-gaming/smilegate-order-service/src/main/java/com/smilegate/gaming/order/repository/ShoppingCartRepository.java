/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.smilegate.gaming.order.repository;

import com.smilegate.gaming.order.model.ShoppingCart;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author THAIN
 */
@Repository
public interface ShoppingCartRepository extends PagingAndSortingRepository<ShoppingCart, String> {

    ShoppingCart findByUserId(String userId);
}
