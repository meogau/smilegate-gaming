/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.smilegate.gaming.order.model.dto;

import lombok.Builder;
import lombok.Data;

/**
 *
 * @author THAIN
 */
@Data
@Builder
public class PaymentRequest {

    private String orderId;
    private Long totalAmount;
    private Long surtax;
    private Long discount;
    private Long moneyToPay;
    private String paymentMethod;
}
