package com.smilegate.gaming.order.controller;

import com.smilegate.gaming.order.enums.OrderStatus;
import com.smilegate.gaming.order.model.OrderData;
import com.smilegate.gaming.order.model.OrderData;
import com.smilegate.gaming.order.model.dto.AuthorizationResponseDTO;
import com.smilegate.gaming.order.model.dto.DetailShoppingCart;
import com.smilegate.gaming.order.service.OrderService;
import com.smilegate.gaming.order.service.ShoppingCartService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import com.thai.nv.message.MessageContent;
import com.thai.nv.message.ResponseMessage;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import com.thai.nv.utils.StringUtil;
import org.springframework.data.domain.Page;

@Controller
public class OrderController extends BaseController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private ShoppingCartService shoppingCartService;

    public ResponseMessage placeOrder(Map<String, String> headerParam, Map<String, Object> bodyRequest) {
        ResponseMessage response;
        AuthorizationResponseDTO userDto = authenToken(headerParam);
        if (userDto == null) {
            response = new ResponseMessage(new MessageContent(HttpStatus.FORBIDDEN.value(), "Bạn chưa đăng nhập", null));
        } else {
            DetailShoppingCart currentCart = shoppingCartService.getCurrentShoppingCart(userDto.getUuid());
            if (currentCart == null || currentCart.getListOrderedGame().isEmpty()) {
                response = new ResponseMessage(new MessageContent(HttpStatus.BAD_REQUEST.value(), "Giỏ hàng rỗng !", null));
            } else {
                long discount = bodyRequest.get("discount") != null ? Long.parseLong(bodyRequest.get("discount").toString()) : 0l;
                long surfax = bodyRequest.get("surfax") != null ? Long.parseLong(bodyRequest.get("surfax").toString()) : 0l;
                OrderData order = orderService.processOrder(userDto.getUuid(), discount, surfax);
                response = new ResponseMessage(new MessageContent(order));
            }
        }
        return response;
    }

    public ResponseMessage getOrderHistory(Map<String, String> headerParam, String urlParam) {
        ResponseMessage response;
        AuthorizationResponseDTO userDto = authenToken(headerParam);
        if (userDto == null) {
            response = new ResponseMessage(new MessageContent(HttpStatus.FORBIDDEN.value(), "Bạn chưa đăng nhập", null));
        } else {
            Map<String, String> urlMap = StringUtil.getUrlParamValues(urlParam);
            int page = urlMap.get("page") != null ? Integer.parseInt(urlMap.get("page")) : 0;
            int size = urlMap.get("size") != null ? Integer.parseInt(urlMap.get("size")) : 5;
            Page<OrderData> orderPage = orderService.getPageOrder(userDto.getUuid(), page, size);
            response = new ResponseMessage(new MessageContent(orderPage.getContent(), orderPage.getTotalElements()));
        }
        return response;
    }

    public ResponseMessage payOrder(Map<String, String> headerParam, Map<String, Object> bodyRequest) {
        ResponseMessage response;
        AuthorizationResponseDTO userDto = authenToken(headerParam);
        if (userDto == null) {
            response = new ResponseMessage(new MessageContent(HttpStatus.FORBIDDEN.value(), "Bạn chưa đăng nhập", null));
        } else {
            String orderId = bodyRequest.get("orderId") != null ? bodyRequest.get("orderId").toString() : "";
            String paymentMethod = bodyRequest.get("paymentMethod") != null ? bodyRequest.get("paymentMethod").toString() : "CASH";
            OrderData orderData = orderService.findById(orderId);
            if (orderData == null) {
                response = new ResponseMessage(new MessageContent(HttpStatus.NOT_FOUND.value(), "Không tìm thấy đơn hàng", null));
            } else {
                if (orderData.getStatus().equals(OrderStatus.SUCCESS)) {
                    response = new ResponseMessage(new MessageContent(HttpStatus.BAD_REQUEST.value(), "Đơn hàng đã được thanh toán", null));
                } else {
                    String errorMessage = orderService.processPayOrder(orderData, paymentMethod);
                    if (!StringUtil.isNullOrEmpty(errorMessage)) {
                        response = new ResponseMessage(new MessageContent(HttpStatus.BAD_REQUEST.value(), errorMessage, null));
                    } else {
                        response = new ResponseMessage(new MessageContent(orderData));
                    }
                }
            }
        }
        return response;
    }

}
