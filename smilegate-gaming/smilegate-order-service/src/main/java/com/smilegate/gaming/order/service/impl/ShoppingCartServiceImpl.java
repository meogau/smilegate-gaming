/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.smilegate.gaming.order.service.impl;

import com.smilegate.gaming.order.enums.CartStatus;
import com.smilegate.gaming.order.model.OrderedGame;
import com.smilegate.gaming.order.model.ShoppingCart;
import com.smilegate.gaming.order.model.dto.AddGameToCartRequest;
import com.smilegate.gaming.order.model.dto.DetailOrderedGame;
import com.smilegate.gaming.order.model.dto.DetailShoppingCart;
import com.smilegate.gaming.order.model.dto.GameProduct;
import com.smilegate.gaming.order.repository.ShoppingCartRepository;
import com.smilegate.gaming.order.service.GameProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.smilegate.gaming.order.service.ShoppingCartService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 *
 * @author THAIN
 */
@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {

    @Autowired
    private ShoppingCartRepository shoppingCartRepository;

    @Autowired
    private GameProductService gameProductService;

    @Override
    public DetailShoppingCart getCurrentShoppingCart(String userId) {
        return transform(getCurrentCart(userId));
    }

    private DetailShoppingCart transform(ShoppingCart cart) {
        DetailShoppingCart detailCart = new DetailShoppingCart();
        detailCart.setId(cart.getId());
        detailCart.setLastUpdate(cart.getLastUpdate());
        detailCart.setStatus(cart.getStatus());
        long totalAmount = 0l;
        List<DetailOrderedGame> listDetailOrderedGames = new ArrayList<>();
        if (!cart.getListOrderedGame().isEmpty()) {
            List<String> listGameIds = cart.getListOrderedGame().stream().map(x -> x.getGameId()).collect(Collectors.toList());
            Map<String, GameProduct> gameMap = gameProductService.getMapGameProductByListGameIds(listGameIds);
            for (OrderedGame orderedGame : cart.getListOrderedGame()) {
                GameProduct gameProduct = gameMap.get(orderedGame.getGameId());
                if (gameProduct != null) {
                    DetailOrderedGame detailOrderedGame = new DetailOrderedGame(gameProduct, orderedGame);
                    listDetailOrderedGames.add(detailOrderedGame);
                    totalAmount += detailOrderedGame.getTotalAmount();
                }
            }
        }
        detailCart.setListOrderedGame(listDetailOrderedGames);
        detailCart.setTotalAmount(totalAmount);
        return detailCart;
    }

    private ShoppingCart getCurrentCart(String userId) {
        ShoppingCart shoppingCart = shoppingCartRepository.findByUserId(userId);
        if (shoppingCart == null) {
            shoppingCart = new ShoppingCart(UUID.randomUUID().toString());
            shoppingCart.setUserId(userId);
            shoppingCart.setStatus(CartStatus.EMPTY);
        }
        return shoppingCart;
    }

    @Override
    public ShoppingCart handleAddNewGame(String userId, AddGameToCartRequest request) {
        ShoppingCart shoppingCart = getCurrentCart(userId);
        boolean isExisted = false;
        for (OrderedGame orderedGame : shoppingCart.getListOrderedGame()) {
            if (orderedGame.getGameId().equalsIgnoreCase(request.getGameId())) {
                int newQuantity = orderedGame.getQuantity() + request.getQuantity();
                if (newQuantity <= 0) {
                    shoppingCart.getListOrderedGame().remove(orderedGame);
                } else {
                    orderedGame.setQuantity(newQuantity);
                }
                isExisted = true;
                break;
            }
        }
        if (!isExisted && request.getQuantity() > 0) {
            OrderedGame orderedGame = new OrderedGame(request.getGameId(), request.getQuantity());
            shoppingCart.getListOrderedGame().add(orderedGame);
        }
        if (!shoppingCart.getListOrderedGame().isEmpty()) {
            shoppingCart.setStatus(CartStatus.SHOPPING);
        } else {
            shoppingCart.setStatus(CartStatus.EMPTY);
        }
        shoppingCart.setLastUpdate(new Date());
        shoppingCartRepository.save(shoppingCart);
        return shoppingCart;
    }

    @Override
    public void resetCart(String cartId) {
        Optional<ShoppingCart> cartOptional = shoppingCartRepository.findById(cartId);
        if (cartOptional.isPresent()) {
            ShoppingCart cart = cartOptional.get();
            cart.setListOrderedGame(new ArrayList<>());
            cart.setStatus(CartStatus.EMPTY);
            cart.setLastUpdate(new Date());
            shoppingCartRepository.save(cart);
        }
    }

}
