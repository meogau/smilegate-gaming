/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.smilegate.gaming.order.service;

import com.smilegate.gaming.order.model.dto.PaymentRequest;

/**
 *
 * @author THAIN
 */
public interface PaymentService {

    boolean payOrder(PaymentRequest paymentRequest);
}
