/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.smilegate.gaming.order.model.dto;

import com.smilegate.gaming.order.model.OrderedGame;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

/**
 *
 * @author THAIN
 */
@Data
public class DetailOrderedGame {

    private String id;

    private String name;

    private String code;

    private String image;

    private Long price;

    private int quantity;

    private long totalAmount;

    public DetailOrderedGame() {

    }

    public DetailOrderedGame(GameProduct gameProduct, OrderedGame orderedGame) {
        this.id = gameProduct.getId();
        this.name = gameProduct.getName();
        this.code = gameProduct.getCode();
        this.image = gameProduct.getImage();
        this.price = gameProduct.getPrice();
        this.quantity = orderedGame.getQuantity();
        this.totalAmount = this.getPrice() * this.getQuantity();
    }

}
