## Smile Gaming System


This project is a microservice-based application using Java language and Spring framework. It includes the following services: Gateway, ID Service, Game Service, Order Service, and Payment Service. The project utilizes Rabbit MQ and Redis technologies to enhance the performance and scalability of the application.

![Smile Gaming Logo](https://gitlab.com/meogau/smilegate-gaming/-/raw/main/diagram.png)
### Folder Structure
The project is structured as follows:

smile-gaming/\
├── gateway/ \
├── id-service/\
├── game-service/\
├── order-service/\
└── payment-service/

### Frameworks
The following frameworks are being used:

Spring Boot\
Spring Data (JPA)\
Spring Security\
### Software Principles and Patterns

The application applies the following software principles and patterns:

- Microservice Architecture: the system is divided into independent services that can be developed and deployed independently.
- Factory Pattern: used to create objects without exposing the instantiation logic.
- Strategy Pattern: used to define a family of algorithms, encapsulate each one, and make them interchangeable.
### Running the Application Locally

To run the application on your local computer, please follow these steps:

1. Clone the project to your local machine.
2. Build the parent project, smile-gaming, which includes all the sub-projects.
3. Run the services in the following order: gateway, id-service, game-service, order-service, and payment-service. 
### Testing the APIs
I have deployed project to my personal server so you can test on local or on my server with address 45.119.84.221\
To test the application , please follow these steps:

1. Import postman the postman file.
2. Create enviroment with 2 variables: host and token
3. Test API in your work space

The detail is in my demo postman video

